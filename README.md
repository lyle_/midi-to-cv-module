# MIDI to CV Module

Code and schematic based upon https://github.com/elkayem/midi2cv/

## Outputs

### Note CV

`NoteCV` outputs a voltage corresponding to a MIDI note (which note it outputs when receiving overlapping MIDI notes is subject to the `Note Priority` setting.

| MIDI Note | Voltage output |
| --------- | -------------- |
| C8 | 7.32V (UPPER LIMIT) |
| C7 | 6.32V |
| C6 | 5.32V |
| C5 | 4.32V |
| C4 | 3.32V |
| C3 | 2.26V |
| C2 | 1.26V |
| C1 | 0.262V (LOWER LIMIT) |

### Clock

The `Clock` output provides a 5V pulse (20ms) for each quarter note beat.

For more info on how MIDI handles clock signals, see https://en.wikipedia.org/wiki/MIDI_beat_clock.

## Trigger

The `Trigger` output provides a 5V pulse (20ms) when the module receives a `NoteOn` message.

## Gate

The `Gate` output provides 5V while any note is being played.

## Velocity

The `Velocity` output provides a voltage corresponding to the velocity data sent with the current note being played, ranging from 0 to 4V.

## Ctrl

`Ctrl` outputs a range from 0 to 4V depending on the incoming MIDI Control Change message.
This can be a user-defined knob or other device on your MIDI controller.
https://electronicmusic.fandom.com/wiki/Continuous_controller

## Pitch Bend

`Pitch Bend` outputs a signal between 0 and 4V corresponding to the position of the pitch bend on your MIDI controller.

## Note Priority

When multiple notes are "on" simultaneously in the MIDI input stream, the `Note Priority` selector determines which note is translated into control voltage.

* Last - the most recently note is used
* Highest - the note with the highest pitch is used
* Lowest- the note with the lowest pitch is used

## Build Notes

Some useful info and possible gotchas when building and setting up this module:

* Program the Arduino before you connect the RX0 pin, as it disrupts programming.
  Alternately, the switch near the bottom of the circuit board disconnects MIDI input in the "down" position.
  This allows programming via the USB interface.
* Reading debugging output via the USB port doesn't work because the RX pin is used by the MIDI hardware.
* The onboard LED doesn't work on the Nano with this code because the SPI code uses pin 13 and interferes.
* Can't use Serial debugging interface because it clashes with I/O (SPI again?)
* For some (probably code-related) reason, Note CV output will not update without also getting a gate change

#### Ardour setup

These are notes specific to setting up Ardour 6.9.0 to work with this module.
YMMV.

##### Clock

When a sequence is running in Ardour, a clock pulse is emitted from the module when the following settings are configured:

* `Edit->Preferences->Transport->Generate->MIDI Beat Clock (Mclk generator)` is enabled
* The Ardour `MIDI Clock out` output in Carla is wired to the JACK input of the MIDI to CV module

##### Note CV

From an Ardour MIDI track, note data is transmitted when the MIDI track's output is connected to the input of the MIDI to CV module's JACK (Carla) input.

